base:
  '*':
    - common.pkgs
    - common.timezone
    - ssh
    - storage
    - networking.hosts
    - networking.firewall.default
    - logging.rsyslog
    - logging.munin_node
    - admin.authorized_keys
  'G@roles:saltmaster':
    - logging.munin_master
  'G@roles:elasticsearch_loadbalancer':
    - haproxy
  'G@roles:elasticsearch':
    - database.elasticsearch
  'G@roles:postgresql*':
    - database.postgres_client
  'G@roles:postgresql_server_master':
    - database.postgres_server
  'G@roles:postgresql_server_slave':
    - database.postgres_server
  'G@roles:postgresql_pgpool':
    - database.postgres_pgpool
  'G@roles:haproxy':
    - haproxy
  'G@roles:www':
    - tardis.web
  'G@roles:rabbitmq':
    - rabbitmq
  'G@roles:celerybeat':
    - tardis.celerybeat
  'G@roles:celeryworker':
    - tardis.celeryworker
  'G@roles:celeryd_filters':
    - tardis.celeryd_filters
  'G@roles:sftpd':
    - tardis.sftpd
    - tardis.mydata_scp
  'G@roles:mydata':
    - tardis.mydata
  'G@roles:filters':
    - tardis.filters
  'G@roles:graylog':
    - logging.graylog
  'G@roles:nrpe':
    - nagios.nrpe
  'G@roles:nagios_core':
    - nagios.nrpe
    - nagios.core
