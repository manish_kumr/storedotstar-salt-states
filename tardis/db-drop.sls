{% if salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound') %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_server_master', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

{% set tardis_db_name = salt['pillar.get']('tardis:db_name', 'mytardis-'+grains['deployment']) %}
{% set pg_admin_user = pillar['postgresql_admin_credentials']['user'] %}
{% set pg_admin_password = pillar['postgresql_admin_credentials']['password'] %}

postgresql-client:
  pkg:
    - installed

tardis_db:
  postgres_database:
    - name: {{ tardis_db_name }}
    - absent
    - db_user: {{ pg_admin_user }}
    - db_password: {{ pg_admin_password }}
    - db_host: {{ tardis_db_host }}
    - db_port: 5432
    - require:
      - pkg: postgresql-client

/home/mytardis/db_restore.log:
  file:
    - absent