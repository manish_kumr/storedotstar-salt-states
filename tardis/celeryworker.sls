include:
  - tardis.base

/etc/supervisor/conf.d/celeryworker.conf:
  file:
    - managed
    - source: salt://tardis/templates/celeryworker.conf
    - template: jinja
    - defaults:
        ncpus: {{ grains['num_cpus'] }}
{% if salt['mine.get']('G@roles:celeryworker and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] == salt['network.interfaces']()['eth0']['inet'][0]['address'] %}
        queues: celery,default,low_priority_queue
{% else %}
        queues: celery,default
{% endif %}
    - require:
        - pkg: supervisor
