include:
  - tardis.users

### cifs ###
cifs-utils:
  pkg:
    - installed

{% for mountpoint, params in pillar.get('cifs-mounts', {}).iteritems() %}
/home/ubuntu/.smb{{ mountpoint }}:
  file.managed:
    - source: salt://storage/templates/smbcredentials
    - template: jinja
    - user: root
    - group: root
    - mode: 600
    - defaults:
        username: {{ params['username'] }}
        password: {{ params['password'] }}

/srv/{{ mountpoint }}:
  mount.mounted:
    - fstype: cifs
    - device: {{ params['share'] }}
    - persist: True
    - mkmnt: True
    - opts: {{ 'ro,' if not params.get('force_rw', False) and (params.get('read_only', True) or grains['deployment'] != 'prod') else '' }}nobootwait,credentials=/home/ubuntu/.smb{{ mountpoint }},uid=mytardis,gid=mytardis
    - require:
        - file: /home/ubuntu/.smb{{ mountpoint }}
        - user: mytardis-user
{% endfor %}

### autofs and davfs ###
davfs2:
  pkg:
    - installed

/srv/autofs_mounts:
  file:
    - directory
    - makedirs: True

autofs:
  pkg:
    - installed
  require:
    - file: /srv/autofs_mounts
  service:
    - running
{% if pillar.get('dav-mounts') or pillar.get('sshfs-mounts') %}
    - watch:
        - file: autofs_*
{% endif %}

{% for mountpoint, params in pillar.get('dav-mounts', {}).iteritems() %}
davfs_secrets_{{ mountpoint }}:
  file:
    - name: /etc/davfs2/secrets
    - append
    - text: {{ params['url'] }}  {{ params['username'] }}  {{ params['password'] }}
    - require:
        - pkg: davfs2

autofs_{{ mountpoint }}:
  file:
    - name: /etc/auto.tardis
    - append
    - text: {{ mountpoint }} -fstype=davfs,rw,dir_mode=0555,file_mode=0666 :'{{ params['url'] }}'
autofs_master_{{ mountpoint }}:
  file:
    - name: /etc/auto.master
    - append
    - text: /srv/autofs_mounts /etc/auto.tardis --timeout=60 --ghost
    - require:
        - pkg: autofs
{% endfor %}

### autofs and sshfs ###
{% if pillar.get('sshfs-mounts') %}
sshfs:
  pkg:
    - installed
autofs_master_sshfs:
  file:
    - name: /etc/auto.master
    - append
    - text: /srv/autofs_mounts /etc/auto.sshfs --timeout=60 --ghost
    - require:
        - pkg: autofs
{% endif %}

{% for mountpoint, params in pillar.get('sshfs-mounts', {}).iteritems() %}
# When changing /etc/auto.sshfs lines, you can delete the old ones with sed,
# by choosing an appropriate pattern to match, e.g.:
# salt '*' cmd.run "sed -i.bak '/kernel_cache :sshfs/d' /etc/auto.sshfs"
autofs_{{ mountpoint }}_entries:
  file:
    - filename: /etc/auto.sshfs
    - accumulated
    {% if not params.get('force_rw', False) and (params.get('read_only', True) or grains['deployment'] != 'prod') %}
    - text: {{ mountpoint }} -fstype=fuse,ro,nodev,nonempty,allow_other,reconnect,workaround=all,uid=mytardis,gid=mytardis,max_read=65536,compression=no,Ciphers=aes128-gcm@openssh.com,auto_cache,no_check_root,kernel_cache,follow_symlinks,transform_symlinks :sshfs\#{{ params['user'] }}@{{ params['host'] }}\:{{ params['remote_path'] }}
    {% else %}
    - text: {{ mountpoint }} -fstype=fuse,rw,nodev,nonempty,allow_other,reconnect,workaround=all,uid=mytardis,gid=mytardis,max_read=65536,compression=no,Ciphers=aes128-gcm@openssh.com,auto_cache,no_check_root,kernel_cache,follow_symlinks,transform_symlinks :sshfs\#{{ params['user'] }}@{{ params['host'] }}\:{{ params['remote_path'] }}
    {% endif %}
    - require_in:
      - file: /etc/auto.sshfs
{% endfor %}

/etc/auto.sshfs:
  file.managed:
    - source: salt://storage/templates/auto.sshfs
    - template: jinja

# For backwards compatibility with old jamesw-massive storage box:
/mnt/sshfs:
  file.symlink:
    - target: /srv/autofs_mounts
