python-gnupg:
  pkg:
    - installed

sysstat:
  pkg:
    - installed

vim:
  pkg:
    - installed

emacs:
  pkg:
    - installed

/etc/vim/vimrc:
  file.managed:
    - source: salt://common/templates/vimrc
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: vim
