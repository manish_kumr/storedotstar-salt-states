# Installs an SSH key that allows all postgres minions to connect to each other
# without a password. Host keys between postgres machines are not checked.

{% if 'postgresql_server_master' in grains['roles'] or 'postgresql_server_slave' in grains['roles'] %}
   {% set requisite = 'postgresql' %}
{% elif 'postgresql_pgpool' in grains['roles'] %}
   {% set requisite = 'pgpool2' %}
{% endif %}

/var/lib/postgresql/.ssh/id_rsa:
  file:
    - managed
    - makedirs: True
    - user: postgres
    - group: postgres
    - mode: 600
    - contents_pillar: postgresql_cluster_key_pair:priv_key
    - require:
        - pkg: {{ requisite }}

/var/lib/postgresql/.ssh/id_rsa.pub:
  file:
    - managed
    - makedirs: True
    - user: postgres
    - group: postgres
    - mode: 644
    - contents_pillar: postgresql_cluster_key_pair:pub_key
    - require:
        - pkg: {{ requisite }}

/var/lib/postgresql/.ssh/authorized_keys:
  file:
    - managed
    - makedirs: True
    - user: postgres
    - group: postgres
    - mode: 644
    - contents_pillar: postgresql_cluster_key_pair:pub_key
    - require:
        - pkg: {{ requisite }}

/var/lib/postgresql/.ssh/config:
  file.managed:
    - source: salt://database/templates/ssh_config
    - template: jinja
    - makedirs: True
    - user: postgres
    - group: postgres
    - mode: 644
    - require:
        - pkg: {{ requisite }}
