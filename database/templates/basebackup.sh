#! /bin/sh
# Recovery script for streaming replication.
# This script assumes that DB node 0 is primary, and 1 is standby.
#
datadir=$1
desthost=$2
destdir=$3
PGCTL=/usr/lib/postgresql/9.3/bin/pg_ctl

psql -c "SELECT pg_start_backup('Streaming Replication', true)" postgres

new_primary_conninfo="primary_conninfo = 'host=$(hostname) port=5432 user=postgres'  # e.g. 'host=localhost port=5432'"
sed -i "s/primary_conninfo =.*/$new_primary_conninfo/" recovery.done

rsync -C -a --delete -e ssh --exclude postgresql.conf --exclude postmaster.pid \
--exclude postmaster.opts --exclude pg_log --exclude pg_xlog \
--exclude recovery.conf $datadir/ $desthost:$destdir/

rm recovery.done

ssh -T $desthost mv $destdir/recovery.done $destdir/recovery.conf

# update salt minion roles
ssh -T $desthost /var/lib/postgresql/set_role_slave
/var/lib/postgresql/set_role_master

psql -c "SELECT pg_stop_backup()" postgres
