nagios3:
  pkg:
    - installed
  service:
    - running

nagiosadmin:
  webutil.user_exists:
    - password: {{ pillar['nagios']['password'] }}
    - htpasswd_file: /etc/nagios3/htpasswd.users
    - options: c
    - force: true
    - require:
      - pkg: nagios3

a2enmod ssl:
  cmd.run:
    - unless: test -f /etc/apache2/mods-enabled/ssl.load
    - require:
      - pkg: apache2

apache2:
  pkg.installed:
    - name: apache2
  service.running:
    - watch:
      - cmd: a2enmod ssl
      - file: /etc/apache2/sites-enabled/000-default-ssl.conf
      - file: /etc/apache2/conf-enabled/nagios3.conf

/etc/apache2/sites-enabled/000-default-ssl.conf:
  file.symlink:
    - target: /etc/apache2/sites-available/default-ssl.conf
    - require:
      - pkg: apache2

/etc/apache2/conf-enabled/nagios3.conf:
  file.symlink:
    - target: /etc/nagios3/apache2.conf
    - require:
        - pkg: apache2

{% for host, data in salt['mine.get']('*', 'network.ipaddrs').items() -%}
/etc/nagios3/conf.d/{{ host }}.cfg:
  file.managed:
    - source: salt://nagios/templates/host_config.cfg
    - template: jinja
    - defaults:
        host: {{ host }}
    - watch_in:
        - service: nagios3
{% endfor %}

/etc/nagios3/conf.d/hostgroups_nagios2.cfg:
  file.managed:
    - source: salt://nagios/templates/hostgroups_nagios2.cfg
    - template: jinja
    - watch_in:
        - service: nagios3

/etc/nagios3/conf.d/extinfo_nagios2.cfg:
  file:
    - absent

/etc/nagios3/conf.d/localhost_nagios2.cfg:
  file:
    - absent

/etc/nagios3/conf.d/contacts_nagios2.cfg:
  file.managed:
    - source: salt://nagios/templates/contacts_nagios2.cfg
    - template: jinja
    - watch_in:
        - service: nagios3
          