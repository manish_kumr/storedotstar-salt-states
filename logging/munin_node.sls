munin-node:
  pkg:
    - installed
  service.running:
    - watch:
        - file: /etc/munin/munin-node.conf

munin-plugins:
  cmd.run:
    - name: "munin-node-configure --shell --families=contrib,auto | sh -x"
    - requires:
        - pkg: munin-node

/etc/munin/munin-node.conf:
  file.managed:
    - source: salt://logging/templates/munin-node.conf
    - require:
        - pkg: munin-node