ssl-cert:
  pkg:
    - installed

cert_file:
  file:
    - name: /etc/ssl/certs/mytardis.pem
    - managed
    - contents_pillar: ssl_cert:{{grains['deployment']}}:cert
    - mode: 644
    - user: root
    - group: root
    - require:
        - pkg: ssl-cert

priv_key_file:
  file:
    - name: /etc/ssl/private/mytardis.key
    - managed
    - contents_pillar: ssl_cert:{{grains['deployment']}}:priv_key
    - mode: 640
    - user: root
    - group: ssl-cert
    - require:
        - pkg: ssl-cert
